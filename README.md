# Mediterraneabus Bot #

Semplice Bot Telegram per la visualizzazione degli orari della Mediterraneabus S.p.A..

Ora i passeggeri della Mediterraneabus potranno controllare in tempo reale gli orari del bus con un semplice messaggio.

![Mediterraneabus](img/logo.png)

## Se il bot vi è stato utile, offrimi una birra ##

[![Donate](https://img.shields.io/badge/Dona-Paypal-blue.svg)](https://paypal.me/fast0n)

------------------------------------------------------------------------------------

This bot is realized with [Telepot](https://github.com/nickoala/telepot).

This is an unofficial bot developed to learn. Mediterraneabus S.p.A. is not responsible in any way.

This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it.